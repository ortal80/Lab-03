# CI/CD with Microsoft VSTS
Lab 03: Deploying build artifacts from VSTS to Artifactory

---

## Preparation

 - Configure a build agent (Lab 01)
 
 - Import the repository:

```
https://oauth2:u9XWV6qNpU7z9PzWRHF5@gitlab.com/build-release-vsts/npm-demo-app.git
```

## Instructions


### Create a Generic Repository

 - Browse to Artifactory and login using the credentials (sela:sela)
 
 - Browse to the "Admin" section and click repositories/local
 
   ![Image 1](images/lab03-1.png)
 
 - Click "New"
 
 - Select "Generic"
 
 - Set the repository key (your-name)
 
  ![Image 2](images/lab03-2.png)
 
 - Click "save & finish"


### Create a Build Definition

 - Browse to the "Build" page and click the button "New"
  
 - Select the build sources

```
Azure Repos Git
```

 - Select the "Empty Job" template

  ![Image 3](images/lab03-3.png)


 - Set the following build variables:

```
ArtifactoryIP: <artifactory-ip>
ArtifactoryPassword: sela (lock)
ArtifactoryUser: sela
ArtifactoryRepository: <your-repo>
```

  ![Image 4](images/lab03-4.png)
  
  
 - Configure CI Trigger:

```
Enable continuous integration
```

  ![Image 5](images/lab03-5.png)
  
 - Set the build name format:

```
Build number format: npm-artifactory_$(Date:yyyyMMdd)$(Rev:.r)
```

  ![Image 6](images/lab03-6.png)
  
 - Select Agent Queue:

```
Sela
```

  ![Image 7](images/lab03-7.png)

 - Add a npm step:

```
Display Name: npm install
Command: install
Working folder with package.json: $(Build.SourcesDirectory)
```

  ![Image 8](images/lab03-8.png)
  
 - Add a npm step:

```
Display Name: Test
Command: custom
Working folder with package.json: $(Build.SourcesDirectory)
Command and arguments: test
```

  ![Image 9](images/lab03-9.png)
  
 - Add a npm build:

```
Display Name: Build
Command: custom
Working folder with package.json: $(Build.SourcesDirectory)
Command and arguments: run build
```

  ![Image 10](images/lab03-10.png)
  
 - Add a Copy Files Task:

```
Display Name: Stage Artifact
Source Folder: $(Build.SourcesDirectory)
Contents: *.zip
Target Folder: $(build.artifactstagingdirectory)
```

  ![Image 11](images/lab03-11.png)
  
 - powershell script:

```
Display Name: Upload to Artifactory
Type: Inline
Script:
```
```
$TargetURL = "http://$Env:ArtifactoryIP/artifactory/$Env:ArtifactoryRepository/nodejs-demoapp-$Env:BuildId.zip"
$FileLocation = "$Env:ArtifactPath\nodejs-demoapp.zip"
$Password = ConvertTo-SecureString $Env:ArtifactoryPassword -AsPlainText -Force
$Credentials = New-Object System.Management.Automation.PSCredential ($Env:ArtifactoryUser,$Password)
Invoke-RestMethod -uri $TargetURL -Method Put -InFile $FileLocation -ContentType "multipart/form-data" -Credential $Credentials
```

  ![Image 12](images/lab03-12.png)
  
- Script Environment Variables:

```
ArtifactoryIP: $(ArtifactoryIP)
BuildId: $(Build.BuildId)
ArtifactPath: $(build.artifactstagingdirectory)
ArtifactoryRepository: $(ArtifactoryRepository)
ArtifactoryPassword: $(ArtifactoryPassword)
ArtifactoryUser: $(ArtifactoryUser)
```

  ![Image 13](images/lab03-13.png)

 - Publish Build Artifacts:
 
```
Display name: Publish Artifact
Path to publish = $(Build.ArtifactStagingDirectory)
Artifact name = drop
Artifact publish location: Visual Studio Team Services/TFS
```

![Image 14](images/lab03-14.png)

 - Save and queue the build
